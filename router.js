const express = require('express')
const router = express.Router();



const user = require('./controller/userController');
const task = require('./controller/taskController');
const profile = require('./controller/profileController');
const authenticate = require('./middlewares/authenticate');
const ownership = require('./middlewares/ownership')
const upload = require('./middlewares/upload')

////////////////////////////////////////////////////////////////////////////////////

router.get('/', (req, res) => {
    res.status(201).json({
        status:' success',
        message: 'Hello World'
    })
  })




//user
router.post('/user/register', user.register);
router.post('/user/login', user.login);

//task
router.get('/task/',authenticate,task.getAll);
router.get('/task/importance',authenticate,task.getImportance);
router.get('/task/completion',authenticate,task.getCompletion);
router.post('/task/',authenticate,task.create);
router.put('/task/:id',authenticate,ownership('task'),task.update)
router.delete('/task/:id', authenticate,ownership('task'),task.deleteTask)

//profile
router.get('/profile/:id',authenticate,profile.getProfile);
router.put('/profile/:id',authenticate,ownership('profile'),upload.single('image'),profile.updateProfile);

//this is for development purpose only
router.put('/profile/dev/:id',authenticate,ownership('profile'),upload.single('image'),profile.updateProfileDev);


module.exports = router;
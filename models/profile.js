'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please input your username'
        },
      }
    },
    address: {
      type: DataTypes.STRING,
      defaultValue: ''
    },
    dob: {
      type: DataTypes.DATE,
      defaultValue: Date.now()
    },
    picture: {
      type: DataTypes.TEXT,
      defaultValue: 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png',
      validate: {
        notEmpty: {
          msg: 'Please input your picture'
        }
      }
    },
    user_id: DataTypes.INTEGER
  }, {});
  profile.associate = function(models) {
    // associations can be defined here
    profile.belongsTo(models.user,{
      foreignKey: 'user_id'
    })
  };
  return profile;
};
const Profile = require('../models').profile;
const success = require('../helper/successHandler');
const imageKit = require('../lib/imagekit');
const { Op } = require('sequelize');


async function getProfile(req, res) {
    try {

        let result = await Profile.findOne({
            where: {
                user_id: req.user.id
            },
            attributes: ['id','name', 'picture']
        })

        res.status(200).json({
            status: 'Success',
            data: result
        })
    }
    catch (err) {
        res.status(422).json({
            message: err.message
        });
    }
}

async function updateProfile(req, res) {
    let file = req.file;
    let {
        name
    } = req.body;

    if (file) {
        try {
            const validFormat = file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/gif';
            if (!validFormat) {
                throw new Error('Wrong Format')
            }
            let image = await imageKit.upload({
                file: file.buffer,
                fileName: file.originalname + '-' + Date.now()
            })
            
            let instance = await Profile.update({
                    name ,
                    picture: image.url 
                
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: 'Success',
                data: image.url
            })


        } catch (err) {

            res.status(422).json({
                status: "Failed",
                message: err.message
            })
        }

    } else {

        try {
            let instance = await Profile.update({
                name
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: 'Success',
                data: `${name} successfully updated`
            })
        } catch (err) {
            res.status(422).json({
                status: "Failed",
                message: err.errors[0].message
            })

        }
    }
}
// this is for development purpose only
async function updateProfileDev(req, res) {
    // let file = req.file;
    // let {
    //     name
    // } = req.body;

    // if (file) {
    //     try {
    //         const validFormat = req.file.mimetype == 'image/png' || req.file.mimetype == 'image/jpg' || req.file.mimetype == 'image/jpeg' || req.file.mimetype == 'image/gif';
    //         if (!validFormat) {
    //             throw new Error('Wrong Format')
    //         }

    //         let instance = await Profile.update({
    //             [Op.or]: [
    //                 { name },
    //                 { picture: file.originalname }
    //             ]
    //         }, {
    //             where: {
    //                 id: req.params.id
    //             }
    //         })
    //         res.status(200).json({
    //             status: 'Success',
    //             data: 'Successfully updated'
    //         })


    //     } catch (err) {

    //         res.status(422).json({
    //             status: "Failed",
    //             message: err.message
    //         })
    //     }

    // } else {

    //     try {
    //         let instance = await Profile.update({
    //             name
    //         }, {
    //             where: {
    //                 id: req.params.id
    //             }
    //         })
    //         res.status(200).json({
    //             status: 'Success',
    //             data: `${name} successfully updated`
    //         })
    //     } catch (err) {
    //         res.status(422).json({
    //             status: "Failed",
    //             message: err.errors[0].message
    //         })

    //     }
    // }

    let file = req.file;
    let {
        name
    } = req.body;

    if (file) {
        try {
            const validFormat = file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/gif';
            if (!validFormat) {
                throw new Error('Wrong Format')
            }
            let image = await imageKit.upload({
                file: file.buffer,
                fileName: file.originalname + '-' + Date.now()
            })
            
            let instance = await Profile.update({
                    name ,
                    picture: image.url 
                
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: 'Success',
                data: image.url
            })


        } catch (err) {

            res.status(422).json({
                status: "Failed",
                message: err.message
            })
        }

    } else {

        try {
            let instance = await Profile.update({
                name
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: 'Success',
                data: `${name} successfully updated`
            })
        } catch (err) {
            res.status(422).json({
                status: "Failed",
                message: err.errors[0].message
            })

        }
    }
}

module.exports = {
    getProfile,
    updateProfile,
    updateProfileDev
}
const Task = require('../models').task;
const User = require('../models').user; 
const success = require('../helper/successHandler');
const { Op } = require('sequelize');


async function create(req, res) {
    try {
        let task = await Task.create({
            title: req.body.title,
            description: req.body.description,
            due_date: req.body.due_date,
            user_id: req.user.id
        })
        success(res, 201, task, "task")
    } catch (err) {
        res.status(422).json({
            status: 'Failed',
            message: err.errors[0].message
        })
    }
}

async function getAll (req, res ) {
    try {
        
        let result = await Task.findAll({
            order: [
                ['due_date', 'Asc']
            ],
            limit: 10,
            where: {
                user_id: req.user.id
            }
        })
    success(res, 200, result, "Search_Result");
    } 
    catch(err) {
        res.status(422).json({
            message: err.message
        });
    }
}
async function getImportance (req, res ) {
    try {
        let result = await Task.findAll({
            where: {
                [Op.and]: {
                    user_id: req.user.id,
                    importance: true
                }      
            },
            order: [
                ['due_date', 'Asc']
            ],
            limit: 10
        })
    success(res, 200, result, "Search_Result");
    } 
    catch(err) {
        res.status(422).json({
            message: err.message
        });
    }
}
async function getCompletion (req, res ) {
    try {
        
        let result = await Task.findAll({
            where: {
                [Op.and]: {
                    user_id: req.user.id,
                    completion: true
                }
                
            },
            order: [
                ['due_date', 'Asc']
            ],
            limit: 10
        })
    success(res, 200, result, "Search_Result");
    } 
    catch(err) {
        res.status(422).json({
            message: err.message
        });
    }
}

async function update(req,res){
    try{
            let instance = await Task.update({
                due_date: req.body.due_date,
                title: req.body.title,
                importance: req.body.importance,
                completion: req.body.completion,
                
            },{
            where: {
                id: req.params.id
                }
            })
            success(res, 200, instance, "task");
        
       
    } catch(err) {   
        res.status(422).json(
            {
                status:"Failed",
                message: err.message
            }
        )
    }
   
}

async function deleteTask(req,res){
    try{
        let instance = await Task.destroy({
            where:{
                id: req.params.id
            }
        })
        let message = `successfully deleted ${req.params.id}`
        success(res,200,message,"task");

    } catch(err){
        res.status(422).json(
            {
                status:"fail",
                message: err.message
            }
        )
    }
}


module.exports = {
    create, getAll, update, deleteTask, getCompletion, getImportance
}
const User = require('../models').user;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// const success = require('../helper/successHandler');
const Profile = require('../models').profile;

async function register(req, res) {
    let {
        email,
        password,
        name
    } = req.body
    try {
        let instance = await User.create({
            email,
            password,
        })
        let profile = await Profile.create({
            name,
            user_id: instance.id
        })
        res.status(200).json({
            status: 'Success',
            data: 
            {
                User: instance,
                Profile: profile
                
            }
        })
    } catch (err) {
        res.status(422).json({
            status: 'Failed',
            message: err.errors[0].message
        })
    }
}

async function login(req, res) {
    try {
        let instance = await User.findOne({
            where: {
                email: req.body.email.toLowerCase()
            }
        })

        if (!instance) {
            throw new Error(`email ${req.body.email} doesn't exist!`)

        }
        let pwd = bcrypt.compareSync(req.body.password, instance.password)
        if (!pwd) {
            throw new Error(`wrong password`)
        }
        const token = jwt.sign({
            id: instance.id,
            email: instance.email
        }, process.env.SECRET_KEY);
     

        res.status(201).json({
            status: 'Success',
            data: {
                token,
                user: instance
            }
        })


    } catch (err) {
        res.status(401).json({
            status: 'Failed',
            message: err.message
        })
    }
}

module.exports = {
    register,
    login
};